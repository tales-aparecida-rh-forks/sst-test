#!/usr/bin/bash

# Intel Speed Select Technology (intel-sst or sst) supports several
# features.  This is a test suite for those features.

# STRESS test shamelessly stolen from David Arcari's Red Hat Platform
# Test Suite

# cpuload.c can be compiled with 'gcc -o cpuload cpuload.c -lm'
STRESS="./cpuload"
STRESSPIDS=
SSTPATH=/usr/bin/

start_load_test()
{
    [ -z "$1" ] && echo "start_load_test: coding error" && exit 1

    cores=$(nproc --all)

    even_only=false
    if [ "$1" -ne "$cores" ]; then
	even_only=true
    fi

    count=0
    for ((i=0; i < cores; ++i)); do
	# if hyperthreading is enabled - only start on the even cores
	[ "$even_only" = true ] && [ $((i % 2)) -eq 1 ] && continue
	[ "$(cat /sys/devices/system/cpu/cpu${i}/online)" -eq 0 ] && continue
	#$STRESS $i &>/dev/null &
	#echo -n "running on cpu $i " && cat /sys/devices/system/cpu/cpu${i}/online
	$STRESS $i &
	STRESSPIDS="$STRESSPIDS ""$!"
	count=$((count + 1))
    done

    # make sure the processes really starts
    sleep 5

    for pid in $STRESSPIDS; do
	result=$(ps -p "$pid" -o comm=)
	size=${#result}

	if [ "$size" -eq 0 ]; then
	    echo "start_load_test: $STRESS is not running!"
	    exit 1
	fi
    done
}

end_load_test()
{
    if [ -n "$STRESSPIDS" ]; then
	for pid in $STRESSPIDS; do
	    kill "$pid"
	    wait "$pid" 2>/dev/null
	done
	STRESSPIDS=
    fi
}

get_random_cpus_on_pkg()
{
	local package
	local numthreads
	local pkg
	local rcpu
	local numentries

	package=$1
	numentries=$2
	numthreads=$((maxthreads + 1))

	while [ $numentries -ne 0 ]; do
		pkg=$((maxpackages + 2))
		while [ $pkg != $package ]; do
			# get a random number between 0 and numthreads
			rcpu=$((RANDOM % numthreads))
			[ "$(cat /sys/devices/system/cpu/cpu"${rcpu}"/online)" -ne 1 ] && continue
			pkg=$(cat /sys/devices/system/cpu/cpu"${rcpu}"/topology/physical_package_id)
		done
		numentries=$((numentries - 1))
		if [ $numentries -eq 0 ]; then
			echo "${rcpu}"
		else
			echo -n "${rcpu},"
		fi
	done
}

# optargs is nice but it only allows character use and not strings ...
while [[ $# > 0 ]]
do
	key="$1"
	shift
	case $key in
	-j|--json)
		JSONFILE=$1
		if [ ! -e "${JSONFILE}" ]; then
			echo "Cannot find ${JSONFILE}"
			exit 1
		fi
		shift;;
	*)
		# unknown option
		echo "options: -j|--json json_file"
		echo ""
		exit 1;;
	esac
done

######################################################################
# System Configuration and Setup Checks
######################################################################

# Setup requirements
# kernel parameter intel_idle.max_cstates=1
if ! grep -E "intel_idle.max_cstate=1" /proc/cmdline >& /dev/null; then
	echo "Depending on your system's BIOS configuration it may be necessary"
	echo " to add intel_idle.max_cstate=1 to kernel parameters and reboot."
fi

# tuned must be set to or include intel_sst add-on profile
if [[ $(cat /etc/tuned/active_profile) != *intel-sst* ]]; then
	echo "Set tuned profile to intel-sst by executing"
	echo "     tuned-adm profile intel-sst"
	echo "and restart the testsuite."
	exit 1
fi

# intel_pstate must be enabled
if [ "$(cpupower frequency-info | grep "driver:" | awk -F ": " ' { print $2 } ')" != "intel_pstate" ]; then
	echo "The intel_pstate driver must be enabled on this platform."
	echo "Re-enable the driver and restart the testsuite."
	exit 1
fi

# in order to test SST-BF, turbo must be disabled
#if [ "$(cat /sys/devices/system/cpu/intel_pstate/no_turbo)" -ne 1 ]; then
#	echo "Turbo must be disbled on this system by executing"
#	echo "     echo 1 > /sys/devices/system/cpu/intel_pstate/no_turbo"
#	echo "and restart the testsuite."
#	#exit 1
#fi

# jq must be available
if [ ! -e /usr/bin/jq ]; then
	echo "The jq bash JSON parser must be installed."
	echo "Install jq and restart the testsuite."
	exit 1
fi

# intel-speed-select better exist
if [ ! -e "${SSTPATH}"intel-speed-select ]; then
	echo "${SSTPATH}intel-speed-select does not exist."
	echo "Please install it and restart the testsuite."
	exit 1
fi

if [ ! -e "${STRESS}" ]; then
	echo "${STRESS} does not exist."
	echo "Execute 'make' to create ${STRESS}."
	exit 1
fi

# get some topology details
maxpackages=$(lscpu | grep "Socket(s):" | awk -F ":" ' { print $2 } ')
maxpackages=$(( maxpackages - 1 ))
maxthreads=$(lscpu | grep "^CPU(s)" | awk -F ":" ' { print $2 } ')
maxthreads=$(( maxthreads - 1 ))

# store max/min to restore everything at the end of the test run
zeromax=$(cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq)
zeromin=$(cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq)

######################################################################
# SST Base Frequency, SST-BF
######################################################################

intel_sst_bf_test() {
	echo "... Running intel SST BF test"

	if ( ! ${SSTPATH}intel-speed-select base-freq enable -a ); then
		echo "'${SSTPATH}intel-speed-select base-freq enable -a' failed"
	fi

	cpulist=$(jq --arg CORE "$CORE" --arg LEVEL perf_profile_level_${level} '.[$CORE][$LEVEL].enable_cpu_list' "${tempdir}"/perf_profile.json | tr "," " " | tr -d "\"")
	[ "$cpulist" == "null" ] && return

	hplist=$(jq --arg CORE "${CORE}" --arg LEVEL perf_profile_level_${level} '.[$CORE][$LEVEL].speed_select_base_freq_properties.high_priority_cpu_list' "${tempdir}"/perf_profile.json | tr "," " " | tr -d "\"")
	[ "$hplist" == "null" ] && return

	hpbf=$(jq --arg CORE "${CORE}" --arg LEVEL perf_profile_level_${level} '.[$CORE][$LEVEL].speed_select_base_freq_properties.high_priority_base_frequencyMHz' "${tempdir}"/perf_profile.json | tr "," " " | tr -d "\"")

	echo "... Testing High Priority cpus ${hplist} with frequency ${hpbf} MHz"
	for cpu in $(seq 0 $maxthreads)
	do
		bf=$(cat /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/base_frequency)
		echo "${bf}" > /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/scaling_max_freq
		echo "${bf}" > /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/scaling_min_freq
	done

	# create some load
	start_load_test "$(nproc --all)"

	# make sure all cpus are honoring base frequency value
	for cpu in ${cpulist}
	do
		[ "$(cat /sys/devices/system/cpu/cpu${cpu}/online)" -eq 0 ] && continue
		bf=$(cat /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/base_frequency)
		curfreq=$(cat /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/scaling_cur_freq)
		delta=$(( curfreq - bf ))
		# absolute value hack ;)
		delta=${delta#-}
		# don't report on high priority threads.  This will be done below.
		echo "${hplist}" | grep -w ${cpu} >& /dev/null
		if [ $? -ne 0 ]; then
			[ "$delta" -gt 10000 ] && echo "ERROR: CPU${cpu} has cpufreq $curfreq but expected $bf."
		fi
	done

	# make sure only the high priority cpus are at high priority frequency.
	# Note: based on the previous test we only have to confirm that the list
	# of high priority cpus is at high priority cpufreq
	for cpu in ${hplist}
	do
		curfreq=$(cat /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/scaling_cur_freq)
		bf=$(( hpbf * 1000 ))
		delta=$(( curfreq - bf ))
		# absolute value hack ;)
		delta=${delta#-}
		[ "$delta" -gt 10000 ] && echo "ERROR: CPU${cpu} has cpufreq $curfreq but expected High Priority frequency $bf."
	done

	end_load_test

	# restore all cpus to their normal frequencies
	for cpu in ${cpulist}
	do
		echo 1 > /sys/devices/system/cpu/cpu"${cpu}"/online
		echo "${zeromax}" > /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/scaling_max_freq
		echo "${zeromin}" > /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/scaling_min_freq
	done

	if ( ! ${SSTPATH}intel-speed-select base-freq disable ); then
		echo "'${SSTPATH}intel-speed-select base-freq disable' failed"
	fi

	echo "... Finished intel SST BF test"
}

get_hpmaxfreq()
{
	hpmaxfreq=$(jq --arg CORE "$CORE" --arg LEVEL perf_profile_level_${level} --arg bucket "bucket_$bucket" '.[$CORE][$LEVEL].speed_select_turbo_freq_properties[$bucket].high_priority_max_frequencyMHz' "${tempdir}"/perf_profile.json | tr -d "\"")
	if [ "$hpmaxfreq" = "null" ]; then
		hpmaxfreq=$(jq --arg CORE "$CORE" --arg LEVEL perf_profile_level_${level} --arg bucket "bucket_$bucket" '.[$CORE][$LEVEL].speed_select_turbo_freq_properties[$bucket].high_priority_max_level_0_frequencyMHz' "${tempdir}"/perf_profile.json | tr -d "\"")
	fi
}

get_lpmaxfreq()
{
	lpmaxfreq=$(jq --arg CORE "$CORE" --arg LEVEL perf_profile_level_${level} '.[$CORE][$LEVEL].speed_select_turbo_freq_properties.speed_select_turbo_freq_clip_frequencies.low_priority_max_frequencyMHz' "${tempdir}"/perf_profile.json | tr -d "\"")
	if [ "$lpmaxfreq" = "null" ]; then
		lpmaxfreq=$(jq --arg CORE "$CORE" --arg LEVEL perf_profile_level_${level} '.[$CORE][$LEVEL].speed_select_turbo_freq_properties.speed_select_turbo_freq_clip_frequencies.low_priority_max_level_0_frequencyMHz' "${tempdir}"/perf_profile.json | tr -d "\"")
	fi
}

intel_sst_tf_test() {
	echo "... Running intel SST TF test"

	for bucket in $(seq 0 4)
	do
		val=$(jq --arg CORE "$CORE" --arg LEVEL perf_profile_level_${level} --arg bucket "bucket_$bucket" '.[$CORE][$LEVEL].speed_select_turbo_freq_properties[$bucket]' "${tempdir}"/perf_profile.json)
		[ "$val" == "null" ] && continue

		get_hpmaxfreq
		hpmaxfreq=$((hpmaxfreq * 1000))
		if [ "$hpmaxfreq" -eq 0 ]; then
			echo "bucket $bucket test abort: High Priority Max Frequency is 0."
			echo "This is a firmware bug and should be reported to the HW vendor."
			continue
		fi

		count=$(jq --arg CORE "$CORE" --arg LEVEL perf_profile_level_${level} --arg bucket "bucket_$bucket" '.[$CORE][$LEVEL].speed_select_turbo_freq_properties[$bucket].high_priority_cores_count' "${tempdir}"/perf_profile.json | tr -d "\"")

		get_lpmaxfreq
		lpmaxfreq=$((lpmaxfreq * 1000))
		[ "$lpmaxfreq" -eq 0 ] && echo "coding error" && exit

		echo "... Testing bucket $bucket hpmax:$hpmaxfreq lpmax:$lpmaxfreq count:$count"

		tfenable=0
		if [ "${sst_tf}" != "enabled" ]; then
			tfenable=1
			countcpulist=$(get_random_cpus_on_pkg "${package}" "${count}")
			echo "... Enabling TF on cpus $countcpulist"
			${SSTPATH}intel-speed-select core-power enable >& /dev/null
			${SSTPATH}intel-speed-select -c ${countcpulist} turbo-freq enable >& /dev/null
			if [ $? -ne 0 ]; then
				echo "WARNING: ${SSTPATH}intel-speed-select -c ${countcpulist} turbo-freq enable failed"
				echo "... Skipping TF test on bucket $bucket"
				continue
			fi
		fi

		# set all cpus to $lpmaxfreq
		for cpu in $(seq 0 $maxthreads)
		do
			[ "$(cat /sys/devices/system/cpu/cpu${cpu}/online)" -eq 0 ] && continue
			echo "${lpmaxfreq}" > /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/scaling_max_freq
			echo "${lpmaxfreq}" > /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/scaling_min_freq
		done

		# create some load
		start_load_test "$(nproc --all)"

		for cpu in $(seq 0 $maxthreads)
		do
			[ "$(cat /sys/devices/system/cpu/cpu${cpu}/online)" -eq 0 ] && continue
			pkg=$(cat /sys/devices/system/cpu/cpu"${cpu}"/topology/physical_package_id)
			[ $package -ne $pkg ] && continue

			topfreq=$(cat /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/scaling_max_freq)
			botfreq=$(cat /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/scaling_min_freq)
			curfreq=$(cat /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/scaling_cur_freq)
			if [ $curfreq -gt $((lpmaxfreq + 10000)) ]; then
				if ! echo "$hpcpulist" | grep $cpu >& /dev/null; then
					echo "ERROR: Low priority CPU $cpu has $curfreq greater than $lpmaxfreq."
				fi
			fi
		done

		end_load_test

		if [ "${tfenable}" -eq 1 ]; then
			#${SSTPATH}intel-speed-select -c 12,13 turbo-freq disable
			${SSTPATH}intel-speed-select core-power disable >& /dev/null
			${SSTPATH}intel-speed-select turbo-freq disable >& /dev/null
		fi

		# restore all cpus to original frequencies
		for cpu in $(seq 0 $maxthreads)
		do
			[ "$(cat /sys/devices/system/cpu/cpu${cpu}/online)" -eq 0 ] && continue
			echo "${zeromax}" > /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/scaling_max_freq
			echo "${zeromin}" > /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/scaling_min_freq
		done
	done

	echo "... Finished intel SST TF test"
}

######################################################################
# MAIN
######################################################################

tempdir=$(mktemp -d)

# get the perf-profile
if [ -e "${JSONFILE}" ]; then
	cp "${JSONFILE}" "${tempdir}"/perf-profile.json
else
	${SSTPATH}intel-speed-select --format json -o "${tempdir}"/perf-profile.json perf-profile info >& /dev/null
fi

# jq has trouble with entries that contain hyphens and curly braces so get rid
# of them
tr "-" "_" < "${tempdir}"/perf-profile.json | tr -d "()" > "${tempdir}"/perf_profile.json

# get the config level lock data
${SSTPATH}intel-speed-select --format json -o "${tempdir}"/locked.json perf-profile get-lock-status >& /dev/null
tr "-" "_" < "${tempdir}"/locked.json > "${tempdir}"/_locked.json

# get the number of performance levels
${SSTPATH}intel-speed-select --format json -o "${tempdir}"/config.json perf-profile get-config-levels >& /dev/null
tr "-" "_" < "${tempdir}"/config.json > "${tempdir}"/_config.json

# go through all package, die, and core combinations
for package in $(seq 0 "$maxpackages")
do
	for die in 0 1
	do
		for core in $(seq 0 "$maxthreads")
		do
			CORE="package_$package:die_$die:cpu_$core"
			# based on, for example,
			#     jq --arg CORE package_1:die_0:cpu_24 '.[$CORE]' ${tempdir}/perf_profile.json
			val=$(jq --arg CORE "${CORE}" '.[$CORE]' "${tempdir}"/perf_profile.json)
			[ "$val" == "null" ] && continue

			echo "Testing $CORE"
			val=$(jq --arg CORE "${CORE}" '.[$CORE].get_lock_status' "${tempdir}"/_locked.json)
			# PRARIT FIX ME: there is only one level for CLX-N
			if ! echo "$val" | grep unlocked >& /dev/null; then
				# run only on the supported config and bail
				levels=0
			else
				# get the number of config levels
				levels=$(jq --arg CORE "${CORE}" '.[$CORE].get_config_levels' "${tempdir}"/_config.json | tr -d "\"")
			fi

			for level in $(seq 0 $levels)
			do
				echo ". Testing level $level"
				if ( ! ${SSTPATH}intel-speed-select perf-profile set-config-level -l "${level}" -o ); then

					# get the current config level
					${SSTPATH}intel-speed-select --format json -o "${tempdir}"/current.json perf-profile get-config-current-level >& /dev/null
					tr "-" "_" < "${tempdir}"/current.json > "${tempdir}"/_current.json
					current_level=$(jq --arg CORE "${CORE}" '.[$CORE].get_config_current_level' "${tempdir}"/_current.json)

					if [ "$current_level" != "\"$level\"" ]; then
						echo "WARNING: ${SSTPATH}intel-speed-select perf-profile set-config-level -l ${level} -o failed"
						echo ". Skipping level $level"
						continue
					fi
				fi
				lscpu | grep 'On-line\|Off-line' | sed '1,$s/^/. /g'

				# what SST features are enabled?
				# speed-select-turbo-freq
				PERF="perf_profile_level_${level}"
				sst_tf=$(jq --arg CORE "${CORE}" --arg PERF "${PERF}" '.[$CORE][$PERF].speed_select_turbo_freq' "${tempdir}"/perf_profile.json | tr -d "\"")
				sst_bf=$(jq --arg CORE "${CORE}" --arg PERF "${PERF}" '.[$CORE][$PERF].speed_select_base_freq' "${tempdir}"/perf_profile.json | tr -d "\"")

				echo ".. LEVEL $level"
				echo "..TF is $sst_tf"
				if [ "${sst_tf}" != "unsupported" ]; then
					intel_sst_tf_test
				else
					echo ".. TF is ${sst_tf} on $CORE level $level: TF test was not run."
				fi
				if [ "${sst_bf}" != "unsupported" ]; then
					intel_sst_bf_test
				else
					echo ".. BF is ${sst_bf} on $CORE level $level: BF test was not run."
				fi
			done
		done
	done
done


# CLEANUP
echo "CLEANUP: Restoring all cpus to factory defaults."
# Restore all cpus
for cpu in $(seq 0 $maxthreads)
do
	echo 1 > /sys/devices/system/cpu/cpu"${cpu}"/online
	echo "${zeromax}" > /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/scaling_max_freq
	echo "${zeromin}" > /sys/devices/system/cpu/cpu"${cpu}"/cpufreq/scaling_min_freq
done
echo "CLEANUP: Removing temporary work directory."
rm -rf /tmp/"${tempdir}"
