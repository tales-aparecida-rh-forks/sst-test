LIBS  = -lm
CFLAGS = -Wall

# Should be equivalent to your list of C files, if you don't build selectively

compile:
	gcc -o cpuload cpuload.c $(CFLAGS) $(LIBS)

run: compile
	./sst-test.sh

clean:
	rm -f ./cpuload
