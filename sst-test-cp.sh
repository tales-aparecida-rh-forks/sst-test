#!/usr/bin/bash

# Intel Speed Select Technology (intel-sst or sst) supports several
# features.  This is a test suite for those features.

# STRESS test shamelessly stolen from David Arcari's Red Hat Platform
# Test Suite

# cpuload.c can be compiled with 'gcc -o cpuload cpuload.c -lm'
STRESS="./cpuload"
STRESSPIDS=
SSTPATH=/usr/bin/

start_load_test()
{
    [ -z "$1" ] && echo "start_load_test: coding error" && exit 1

    cores=$(nproc --all)

    even_only=false
    if [ "$1" -ne "$cores" ]; then
	even_only=true
    fi

    count=0
    for ((i=0; i < cores; ++i)); do
	# if hyperthreading is enabled - only start on the even cores
	[ "$even_only" = true ] && [ $((i % 2)) -eq 1 ] && continue
	[ "$(cat /sys/devices/system/cpu/cpu${i}/online)" -eq 0 ] && continue
	#$STRESS $i &>/dev/null &
	#echo -n "running on cpu $i " && cat /sys/devices/system/cpu/cpu${i}/online
	$STRESS $i &
	STRESSPIDS="$STRESSPIDS ""$!"
	count=$((count + 1))
    done

    # make sure the processes really starts
    sleep 5

    for pid in $STRESSPIDS; do
	result=$(ps -p "$pid" -o comm=)
	size=${#result}

	if [ "$size" -eq 0 ]; then
	    echo "start_load_test: $STRESS is not running!"
	    exit 1
	fi
    done
}

end_load_test()
{
    if [ -n "$STRESSPIDS" ]; then
	for pid in $STRESSPIDS; do
	    kill "$pid"
	    wait "$pid" 2>/dev/null
	done
	STRESSPIDS=
    fi
}

get_random_cpus_on_pkg()
{
	local package
	local numthreads
	local pkg
	local rcpu
	local numentries

	package=$1
	numentries=$2
	numthreads=$((maxthreads + 1))

	while [ $numentries -ne 0 ]; do
		pkg=$((maxpackages + 2))
		while [ $pkg != $package ]; do
			# get a random number between 0 and numthreads
			rcpu=$((RANDOM % numthreads))
			[ "$(cat /sys/devices/system/cpu/cpu"${rcpu}"/online)" -ne 1 ] && continue
			pkg=$(cat /sys/devices/system/cpu/cpu"${rcpu}"/topology/physical_package_id)
		done
		numentries=$((numentries - 1))
		if [ $numentries -eq 0 ]; then
			echo "${rcpu}"
		else
			echo -n "${rcpu},"
		fi
	done
}

# optargs is nice but it only allows character use and not strings ...
while [[ $# > 0 ]]
do
	key="$1"
	shift
	case $key in
	-j|--json)
		JSONFILE=$1
		if [ ! -e "${JSONFILE}" ]; then
			echo "Cannot find ${JSONFILE}"
			exit 1
		fi
		shift;;
	*)
		# unknown option
		echo "options: -j|--json json_file"
		echo ""
		exit 1;;
	esac
done

######################################################################
# System Configuration and Setup Checks
######################################################################

# Setup requirements
# kernel parameter intel_idle.max_cstates=1
if ! grep -E "intel_idle.max_cstate=1" /proc/cmdline >& /dev/null; then
	echo "Add intel_idle.max_cstate=1 to kernel parameters and reboot."
	exit 1
fi

# tuned must be set to or include intel-sst add-on profile
if [[ $(cat /etc/tuned/active_profile) != *intel-sst* ]]; then
	echo "Set tuned profile to intel-sst by executing"
	echo "     tuned-adm profile intel-sst"
	echo "and restart the testsuite."
	exit 1
fi

# intel-pstate will interfere with CP functionality
if [ -e /sys/devices/system/cpu/cpu0/cpufreq ]; then
	echo "The intel-pstate driver must be disabled."
	echo "Add intel_pstate=disable to kernel parameters and reboot,"
	echo "or execute 'rmmod acpi-cpufreq'"
	exit 1
fi

# jq must be available
if [ ! -e /usr/bin/jq ]; then
	echo "The jq bash JSON parser must be installed."
	echo "Install jq and restart the testsuite."
	exit 1
fi

# intel-speed-select better exist
if [ ! -e "${SSTPATH}"intel-speed-select ]; then
	echo "${SSTPATH}intel-speed-select does not exist."
	echo "Please install it and restart the testsuite."
	exit 1
fi

if [ ! -e "${STRESS}" ]; then
	echo "${STRESS} does not exist."
	echo "Execute 'make' to create ${STRESS}."
	exit 1
fi

# get some topology details
maxpackages=$(lscpu | grep "Socket(s):" | awk -F ":" ' { print $2 } ')
maxpackages=$(( maxpackages - 1 ))
maxthreads=$(lscpu | grep "^CPU(s)" | awk -F ":" ' { print $2 } ')
maxthreads=$(( maxthreads - 1 ))

######################################################################
# SST Core Power, SST-CP
######################################################################

intel_sst_cp_test() {

	basefreq=$(jq --arg CORE "${CORE}" --arg PERF "${PERF}" '.[$CORE][$PERF].base_frequencyMHz' "${tempdir}"/perf_profile.json | tr -d "\"")

	# set proportional
	${SSTPATH}intel-speed-select core-power enable -p 1 >& /dev/null
	[ $? -ne 0 ] && echo "ERROR: CP not enabled"

	for clos in $(seq 0 3)
	do
		${SSTPATH}intel-speed-select core-power config -c "${clos}" --min "${basefreq}"  --epp 0 --weight 0 >& /dev/null
	done

	closdata="${tempdir}"/clos"${level}".data
	echo > $closdata
	for cpu in $(seq 0 $maxthreads)
	do
		[ "$(cat /sys/devices/system/cpu/cpu"${cpu}"/online)" -eq 0 ]&& continue
		numclos=$((RANDOM % 4 ))
		# is this cpu already assigned?
		if grep "^${cpu} " $closdata >& /dev/null; then
			continue
		fi

		echo "$cpu $numclos" >> $closdata
		# The clos assignment affects the core so also have to
		# assign the thread's sibling
		siblings=$(cat /sys/devices/system/cpu/cpu"${cpu}"/topology/thread_siblings_list | tr -d "," | tr "-" " ")
		sibling=( "$siblings" )
		sibling=( "${sibling[@]/$cpu}" )
		echo "${sibling[@]} $numclos" >> $closdata

		${SSTPATH}intel-speed-select -c "${cpu}" core-power assoc -c "${numclos}" >& /dev/null
	done

	# add load
	start_load_test "$(nproc --all)"
	turbostat --quiet --show CPU,Bzy_MHz -i 1 --num_iterations 1 | sort -n | grep -v "CPU\|-" >& "${tempdir}"/turbostat"${level}".data

	end_load_test

	echo > "${tempdir}"/cp"${level}".result
	cat $closdata | while read entry
	do
		cpu=$(echo "$entry" | awk ' { print $1 } ')
		clos=$(echo "$entry" | awk ' { print $2 } ')
		freq=$(cat "${tempdir}"/turbostat"${level}".data | tr "\t" "," | grep "^${cpu}" | awk -F "," ' { print $2 } ')
		echo "$clos $freq $cpu" >> "${tempdir}"/cp"${level}".result
	done

	# compare cpufreqs.  0 > 1 > 2 > 3
	for clos in $(seq 0 3)
	do
		average=0
		count=0
		cat "${tempdir}"/cp"${level}".result | grep "^${clos} " | awk ' { print $2 } ' | while read value
		do
			average=$((average + value))
			count=$((count + 1))
		echo "$clos $count $average"
		done | tail -1 | while read values
			do
				clos=$(echo $values | awk ' { print $1 } ')
				count=$(echo $values | awk ' { print $2 } ')
				ave=$(echo $values | awk ' { print $3 } ')

				ave=$(( ave / count ))
				echo "clos ${clos} has ${count} threads with average freq ${ave}"
			done
	done

	${SSTPATH}intel-speed-select core-power disable >& /dev/null
	[ $? -ne 0 ] && echo "ERROR: CP not disabled"
	return
}

######################################################################
# MAIN
######################################################################

tempdir=$(mktemp -d)

# get the perf-profile
if [ -e "${JSONFILE}" ]; then
	cp "${JSONFILE}" "${tempdir}"/perf-profile.json
else
	${SSTPATH}intel-speed-select --format json -o "${tempdir}"/perf-profile.json perf-profile info >& /dev/null
fi

# jq has trouble with entries that contain hyphens and curly braces so get rid
# of them
tr "-" "_" < "${tempdir}"/perf-profile.json | tr -d "()" > "${tempdir}"/perf_profile.json

# get the config level lock data
${SSTPATH}intel-speed-select --format json -o "${tempdir}"/locked.json perf-profile get-lock-status >& /dev/null
tr "-" "_" < "${tempdir}"/locked.json > "${tempdir}"/_locked.json

# get the number of performance levels
${SSTPATH}intel-speed-select --format json -o "${tempdir}"/config.json perf-profile get-config-levels >& /dev/null
tr "-" "_" < "${tempdir}"/config.json > "${tempdir}"/_config.json

# go through all package, die, and core combinations
for package in $(seq 0 "$maxpackages")
do
	for die in 0 1
	do
		for core in $(seq 0 "$maxthreads")
		do
			CORE="package_$package:die_$die:cpu_$core"
			# based on, for example,
			#     jq --arg CORE package_1:die_0:cpu_24 '.[$CORE]' ${tempdir}/perf_profile.json
			val=$(jq --arg CORE "${CORE}" '.[$CORE]' "${tempdir}"/perf_profile.json)
			[ "$val" == "null" ] && continue

			echo "Testing $CORE"
			val=$(jq --arg CORE "${CORE}" '.[$CORE].get_lock_status' "${tempdir}"/_locked.json)
			# PRARIT FIX ME: there is only one level for CLX-N
			if ! echo "$val" | grep unlocked >& /dev/null; then
				echo "TDP Lock status is locked"
				# this cannot be true.  CP is not supported on CLX-N
			fi

			# get the number of config levels
			levels=$(jq --arg CORE "${CORE}" '.[$CORE].get_config_levels' "${tempdir}"/_config.json | tr -d "\"")

			for level in $(seq 0 $levels)
			do
				echo ". Testing level $level"
				if ( ! ${SSTPATH}intel-speed-select perf-profile set-config-level -l "${level}" -o ); then
					# get the current config level
					${SSTPATH}intel-speed-select --format json -o "${tempdir}"/current.json perf-profile get-config-current-level >& /dev/null
					tr "-" "_" < "${tempdir}"/current.json > "${tempdir}"/_current.json
					current_level=$(jq --arg CORE "${CORE}" '.[$CORE].get_config_current_level' "${tempdir}"/_current.json)

					if [ "$current_level" != "\"$level\"" ]; then
						echo "WARNING: ${SSTPATH}intel-speed-select perf-profile set-config-level -l ${level} -o failed"
						echo ". Skipping level $level"
						continue
					fi
				fi
				lscpu | grep 'On-line\|Off-line' | sed '1,$s/^/. /g'

				# what SST features are enabled?
				# speed-select-turbo-freq
				PERF="perf_profile_level_${level}"
				sst_cp=$(jq --arg CORE "${CORE}" --arg PERF "${PERF}" '.[$CORE][$PERF].speed_select_core_power' "${tempdir}"/perf_profile.json | tr -d "\"")

				echo ".. LEVEL $level"
				echo ".. CP is $sst_cp"
				if [ "${sst_cp}" != "unsupported" ]; then
					intel_sst_cp_test
				else
					echo ".. TF is ${sst_tf} on $CORE level $level: TF test was not run."
				fi
			done
		done
	done
done

# CLEANUP
echo "CLEANUP: Restoring all cpus to factory defaults."
# Restore all cpus
for cpu in $(seq 0 $maxthreads)
do
	echo 1 > /sys/devices/system/cpu/cpu"${cpu}"/online
done
echo "CLEANUP: Removing temporary work directory."
rm -rf /tmp/"${tempdir}"
